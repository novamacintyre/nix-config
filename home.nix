{ config, pkgs, ... }:

{
  home.username = "nova";
  home.homeDirectory = "/home/nova";

  programs.git = {
    enable = true;
    userName = "Nova MacIntyre";
    userEmail = "nova@novamacintyre.dev";
  };

  nixpkgs.config.allowUnfree = true;

  home.packages = with pkgs; [
    chromium
    dbeaver
    deluge
    firefox
    freecad
    gimp
    glow
    gnome.gnome-tweaks
    inkscape
    jq
    jetbrains.pycharm-professional
    jetbrains.webstorm
    keepassxc
    signal-desktop
    thunderbird
    tree
    which
    wget
    wireshark-qt
    yubikey-manager
    yubikey-manager-qt
    yubikey-agent
    yubikey-personalization
    yubikey-personalization-gui
    zotero
  ];

  programs.bash = {
    enable = true;
    enableCompletion = true;
  };

  home.stateVersion = "24.05";

  programs.home-manager.enable = true;

}
